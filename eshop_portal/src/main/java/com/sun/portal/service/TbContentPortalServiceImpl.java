package com.sun.portal.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.sun.commons.pojo.BigAdvertisement;
import com.sun.commons.utils.JsonUtils;
import com.sun.dubbo.webmanage.service.TbContentDubboService;
import com.sun.redis.dao.RedisDao;
import com.sun.webmanage.model.TbContent;

@Service("TbContentPoatalService")
public class TbContentPortalServiceImpl implements TbContentPortalService{

	@Reference
	private TbContentDubboService tbContentDubboService;
	@Resource
	private RedisDao redisDao;
	@Value("${portal.cache.big.advertisement}")
	private String adkey;
	
	@Override
	public String listBigAdvertisement() {
		if(redisDao.exists(adkey)){
			return redisDao.getKey(adkey);
		}else{
			List<TbContent> listdata = tbContentDubboService.listTbContentLimit(89,6, true);
			List<BigAdvertisement> listAd = new ArrayList<>();
			for(TbContent tb:listdata){
				BigAdvertisement ad = new BigAdvertisement();
				ad.setId(tb.getId());
				ad.setSrc(tb.getPic());
				ad.setHref(tb.getUrl());
				ad.setAlt(tb.getTitle());
				listAd.add(ad);
			}
			String jsonstr = JsonUtils.objectToJson(listAd);
			redisDao.setKey(adkey, jsonstr);
			return jsonstr;
		}
	}

}
