package com.sun.cart.interceptor;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.sun.cart.service.CommonConfig;
import com.sun.commons.utils.CookieUtils;
import com.sun.commons.utils.HttpClientUtil;
import com.sun.commons.utils.JsonUtils;
import com.sun.redis.dao.RedisDao;
import com.sun.webmanage.model.TbUser;

public class LoginInterceptor extends HandlerInterceptorAdapter{
	
	@Resource
	private RedisDao redisDao;
	@Resource
	private CommonConfig commonConfig;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		//所有商品详情的访问都经过这个拦截器，这里来判断用户是否登陆过
		String token = CookieUtils.getCookieValue(request, "TT_TOKEN");
		String href = commonConfig.passport_login_href;
		if(token==null || token.equals("")){//cookie中没有token
			response.sendRedirect(href);
			return false;
		}
		
		String result = HttpClientUtil.doPost(commonConfig.passport_usertoken_href+token);
		@SuppressWarnings("unchecked")
		Map<String,Object> resMap = JsonUtils.jsonToPojo(result, Map.class);
		String status = resMap.get("status").toString();
		if(status.equals("200")){
			return true;
		}
		
		StringBuffer tarUrl = request.getRequestURL();
		tarUrl.append("%3F");//转义?
		tarUrl.append("num%3D");//转义=
		tarUrl.append(request.getParameter("num"));
		//System.out.println("redirect:==="+href+"?cartUrl="+tarUrl.toString());
		//登录成功后会再次跳转到目标控制器
		response.sendRedirect(href+"?cartUrl="+tarUrl.toString());
		//redirect:===http://localhost:8084/user/showLogin?cartUrl=http://localhost:8085/cart/add/1037960766.html%3Fnum%3D1
		return false;
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		//request.setAttribute("key", "如果能获取到，证明postHandle方法在controller返回之前执行");
		super.postHandle(request, response, handler, modelAndView);
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}
	

	/*private <T> T getBeansUtil(Class<T> clazz, HttpServletRequest request){
	        WebApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
	        return applicationContext.getBean(clazz);
	}*/
	
}
/**
 * Spring拦截器
HandlerInterceptorAdapter需要继承，HandlerInterceptor需要实现
可以作为日志记录和登录校验来使用
建议使用HandlerInterceptorAdapter，因为可以按需进行方法的覆盖。
主要为3种方法：
preHandle
//进入 Handler方法之前执行
//用于身份认证、身份授权
//比如身份认证，如果认证通过表示当前用户没有登陆，需要此方法拦截不再向下执行
postHandle
//进入Handler方法之后，返回modelAndView之前执行
//应用场景从modelAndView出发：将公用的模型数据(比如菜单导航)在这里传到视图，也可以在这里统一指定视图
afterCompletion
//执行Handler完成执行此方法
//应用场景：统一异常处理，统一日志处理
简介
SpringWebMVC的处理器拦截器，类似于Servlet开发中的过滤器Filter，用于处理器进行预处理和后处理。

应用场景
1、日志记录，可以记录请求信息的日志，以便进行信息监控、信息统计等。
2、权限检查：如登陆检测，进入处理器检测是否登陆，如果没有直接返回到登陆页面。
3、性能监控：典型的是慢日志
 * */
