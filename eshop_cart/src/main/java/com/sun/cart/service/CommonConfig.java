package com.sun.cart.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CommonConfig{

	@Value("${passport.login.href}")
	public String passport_login_href;
	@Value("${passport.usertoken.href}")
	public String passport_usertoken_href;
	
	
}
