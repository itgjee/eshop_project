package com.sun.webmanage.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sun.webmanage.service.TestService;

@Controller
public class IndexController {
	
	@Resource
	private TestService testService;

	@RequestMapping("/")
	public String welcome(){
		return "index";
	}
	
	@RequestMapping("{page}")
	public String showPage(@PathVariable String page){
		return page;
	}
	
}
