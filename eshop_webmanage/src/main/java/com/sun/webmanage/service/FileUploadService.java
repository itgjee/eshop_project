package com.sun.webmanage.service;

import org.springframework.web.multipart.MultipartFile;

public interface FileUploadService {

	/**
	 * 图片上传
	 * @param file
	 * @return 上传成功返回图片访问路径   上传失败返回空字符串
	 */
	String fileUpload(MultipartFile file);
}
