package com.sun.webmanage.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.sun.commons.utils.HttpClientUtil;
import com.sun.commons.utils.JsonUtils;

@Service("CacheManageService")
public class CacheManageServiceImpl implements CacheManageService{
	
	@Value("${search.cache.init}")
	private String solrInitUrl;  
	//http://localhost:8083/solr/init

	@Override
	public Map<String, Object> reflushSolrCache(boolean is_all) {
		Map<String,String> param = new HashMap<>(8);
		param.put("is_all", is_all?"true":"false");
		String result = HttpClientUtil.doPost(solrInitUrl, param);
		//{"data":"更新条目数：3500,操作时间：77s","status":200,"info":"同步solr索引信息成功"}
		@SuppressWarnings("unchecked")
		Map<String, Object> resultMap = JsonUtils.jsonToPojo(result, Map.class);
		System.out.println("ajsldajsdla");
		return resultMap;
	}

	
}
