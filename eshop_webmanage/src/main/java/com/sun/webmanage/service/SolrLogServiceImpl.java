package com.sun.webmanage.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.sun.commons.pojo.EasyUIDataGrid;
import com.sun.dubbo.webmanage.service.TbSolrSyncLogDubboService;

@Service("SolrSynService")
public class SolrLogServiceImpl implements SolrLogService{

	@Reference
	private TbSolrSyncLogDubboService TbSolrSyncLogDubboService;
	
	private final static Logger LOGGER = LoggerFactory.getLogger(SolrLogService.class);

	@Override
	public EasyUIDataGrid listSolrLog(int pager, int rows) {
		return TbSolrSyncLogDubboService.selectSolrSyncLogList(pager, rows, null);
	}
	
	
}
