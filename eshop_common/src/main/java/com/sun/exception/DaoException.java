package com.sun.exception;

import java.io.Serializable;

/**
 * 自定义数据传输异常
 * @author 孙哈哈
 *
 */
public class DaoException extends Exception implements Serializable{

	public DaoException(String message){
		super(message);
	}
	
}
