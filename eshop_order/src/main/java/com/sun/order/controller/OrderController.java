package com.sun.order.controller;

import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sun.commons.pojo.TbItemVO;
import com.sun.order.pojo.CreateOrderVO;
import com.sun.order.service.OrderService;

@Controller
public class OrderController {

	@Resource
	private OrderService orderService;
	
	private static Logger LOGGER = LoggerFactory.getLogger(OrderController.class);
	
	@RequestMapping("/order/order-cart.html")
	public String orderPage(HttpServletRequest request,Model retmodel,Long[] id){
		List<TbItemVO> catList = orderService.catList(request, id);
		retmodel.addAttribute("cartList", catList);
		return "order-cart";
	}
	
	@RequestMapping("/order/create.html")
	public String createOrder(HttpServletRequest request,CreateOrderVO createOrderVO,Model retmodel){
		try {
			orderService.createOrder(request, createOrderVO);
		} catch (Exception e) {
			LOGGER.error("生成订单信息"+e.getMessage());
			e.printStackTrace();
			return "error/exception";
		}
		//利用形参传值
		retmodel.addAttribute("orderId", createOrderVO.getTbOrder().getOrderId());
		retmodel.addAttribute("payment", createOrderVO.getPayment());
		Calendar cal = Calendar.getInstance();
		cal.setTime(createOrderVO.getOrderShipping().getCreated());
		cal.add(Calendar.DATE, 3);//默认生成订单三日内送达
		retmodel.addAttribute("date", cal.getTime());
		return "success";
	}
	
}
