package com.sun.passport.service;

import java.util.Map;

import com.sun.webmanage.model.TbUser;

public interface TbUserService {

	TbUser loadTbUserByUsernameAndPassword(String username,String password);
	
	boolean saveTbUser(TbUser tbUser);
	
	Map<String,Object> getUserInfoByToken(String token);
}
