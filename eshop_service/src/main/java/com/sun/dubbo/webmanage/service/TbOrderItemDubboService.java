package com.sun.dubbo.webmanage.service;

import com.sun.webmanage.model.TbOrderItem;

public interface TbOrderItemDubboService {

	boolean insertTbOrderItem(TbOrderItem tbOrderItem);
	
	boolean updateTbOrderItem(TbOrderItem tbOrderItem);
	
}
