package com.sun.dubbo.webmanage.service;

import java.util.List;

import com.sun.commons.pojo.EasyUIDataGrid;
import com.sun.webmanage.model.TbItemParam;

public interface TbItemParamDubboService {

	EasyUIDataGrid listTbItemParamPage(int pager,int rows);
	
	int delTbItemParamByPK(long id);
	
	List<TbItemParam> loadParamByCatId(long catId);
	
	int saveTbItemParam(TbItemParam tbItemParam);
	
	
}
