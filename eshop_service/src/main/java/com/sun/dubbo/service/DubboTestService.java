package com.sun.dubbo.service;

import java.util.List;

import com.sun.webmanage.model.TbContentCategory;

public interface DubboTestService {
	
	List<TbContentCategory> getInfo();
}
